import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:3000",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default {
  getRoles() {
    return apiClient.get("/roles");
  },
  getRole(id) {
    return apiClient.get("/roles/" + id);
  },
  postRole(role) {
    return apiClient.post("/roles", role);
  },
};
