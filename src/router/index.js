import Vue from "vue";
import VueRouter from "vue-router";
import ErrorDisplay from "@/views/ErrorDisplay.vue";
import Home from "@/views/Home.vue";
import RoleDetails from "@/views/RoleDetails.vue";
import RoleForm from "@/views/RoleForm.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/role/:id",
    name: "RoleDetails",
    props: true,
    component: RoleDetails,
  },
  {
    path: "/role/change",
    name: "RoleForm",
    component: RoleForm,
  },
  {
    path: "/error/:error",
    name: "ErrorDisplay",
    props: true,
    component: ErrorDisplay,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
