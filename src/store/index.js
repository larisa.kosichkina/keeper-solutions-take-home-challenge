import Vue from "vue";
import Vuex from "vuex";
import RoleService from "@/services/RoleService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    roles: [],
    role: null,
  },
  mutations: {
    ADD_ROLE(state, role) {
      state.roles.push(role);
    },
    SET_ROLE(state, role) {
      state.role = role;
    },
    SET_ROLES(state, roles) {
      state.roles = roles;
    },
  },
  actions: {
    createRole({ commit }, role) {
      return RoleService.postRole(role)
        .then(() => {
          commit("ADD_ROLE", role);
          commit("SET_ROLE", role);
        })
        .catch((error) => {
          throw error;
        });
    },
    fetchRoles({ commit }) {
      return RoleService.getRoles()
        .then((response) => {
          commit("SET_ROLES", response.data);
        })
        .catch((error) => {
          throw error;
        });
    },
    fetchRole({ commit, getters }, id) {
      const role = getters.getRoleById(id);
      if (role) {
        commit("SET_ROLE", role);
      } else {
        return RoleService.getRole(id)
          .then((response) => {
            commit("SET_ROLE", response.data);
          })
          .catch((error) => {
            throw error;
          });
      }
    },
  },
  getters: {
    getRoleById: (state) => (id) => {
      return state.roles.find((role) => role.id === id);
    },
  },
  modules: {},
});
